# ApimageFront

The purpose of this project is to extract metadata from pictures. (Front-end of Apimage).

## Technology

* Angular 8

## Build

ng serve --proxy-config proxy.config.json --open

## Changelog

**1.0.1:**
Some graphical fix.

**1.0.0 (final release):**
All function from the back is used in the front.
