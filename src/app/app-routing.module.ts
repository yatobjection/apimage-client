import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';
import { DisplaymetadataComponent } from './displaymetadata/displaymetadata.component';
import { MetadatalistComponent } from './metadatalist/metadatalist.component';
import { SearchclientComponent } from './searchclient/searchclient.component';
import { EditAccountComponent } from './edit-account/edit-account.component';

const routes: Routes = [
  { path: 'signup', component: SignupComponent },
  { path: 'login', component: LoginComponent },
  { path: 'displaymetadata', component: DisplaymetadataComponent },
  { path: 'metadatalist', component: MetadatalistComponent },
  { path: 'searchclient', component: SearchclientComponent },
  { path: 'editaccount', component: EditAccountComponent }  
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
