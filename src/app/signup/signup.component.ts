import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { IndataService } from '../indata.service';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { Session } from '../session';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  signupForm: FormGroup;

  constructor(private indataService: IndataService, private fb: FormBuilder, private snackBar: MatSnackBar, public router: Router, private session: Session) { }

  ngOnInit() {
    this.signupForm = this.fb.group({
      lastName: [],
      firstName: [],
      age: [],
      sex: [],
      phone: [],
      address: [],
      mail: [],
      password: []
    });
  }

  onSignup() {
    this.indataService.newClient(this.signupForm.value).subscribe(client => {
      console.log(client);
      if (client) {
        console.log(this.signupForm);
        this.signupForm.reset();
        this.snackBar.open("Compte créé avec succès.", null, {
          duration: 2000,
        });
        this.router.navigate(['/login']);
      }
      else {
        this.snackBar.open("Adresse email déjà existant.", null, {
          duration: 2000,
        });
      }
    });
  }

}
