import { Injectable } from "@angular/core";

@Injectable()
export class Globals {
    isClient:boolean = null;
    isChoosed:boolean = false;
    loading:boolean = false;
}