import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { IndataService } from '../indata.service';
import { MatSnackBar } from '@angular/material';
import { Session } from '../session';
import { Router } from '@angular/router';
import { Globals } from '../globals';

@Component({
  selector: 'app-edit-account',
  templateUrl: './edit-account.component.html',
  styleUrls: ['./edit-account.component.css']
})
export class EditAccountComponent implements OnInit {

  editForm: FormGroup;

  constructor(private fb: FormBuilder, private indataService: IndataService, private session: Session, private snackBar: MatSnackBar, private globals: Globals, public router: Router) { }

  ngOnInit() {
    this.globals.loading = true;
    this.editForm = this.fb.group({
      lastName: [],
      firstName: [],
      age: [],
      sex: [],
      phone: [],
      address: [],
      mail: [],
      password: []
    });
    if (this.session.userId) {
      this.indataService.getUser(this.session.userId.toString()).subscribe(user => {
        console.log(user);
        this.editForm = this.fb.group({
          userId: [user.userId],
          lastName: [user.lastName],
          firstName: [user.firstName],
          age: [user.age],
          sex: [user.sex],
          phone: [user.phone],
          address: [user.address],
          mail: [user.mail],
          password: [user.password]
        });
        this.globals.loading = false;
      }) 
    }
    else {
      this.indataService.getClient(this.session.clientId.toString()).subscribe(client => {
        console.log(client);
        this.editForm = this.fb.group({
          clientId: [client.clientId],
          lastName: [client.lastName],
          firstName: [client.firstName],
          age: [client.age],
          sex: [client.sex],
          phone: [client.phone],
          address: [client.address],
          mail: [client.mail],
          password: [client.password]
        });
        this.globals.loading = false;
      })
    }
  }

  onEdit() {
    if (this.session.userId) {
      this.indataService.editUser(this.editForm.value).subscribe(() => {
        this.snackBar.open("Modifications prises en compte.", null, {
          duration: 2000,
        });
      })
    }
    else {
      this.indataService.editClient(this.editForm.value).subscribe(() => {
        this.snackBar.open("Modifications prises en compte.", null, {
          duration: 2000,
        });
      });
    }
  }

  onDisable() {
    this.editForm.value.active = false;
    this.indataService.editClient(this.editForm.value).subscribe(() => {
      console.log(this.editForm.value);
      this.snackBar.open("Compte supprimé.", null, {
        duration: 2000,
      });
      this.session.clientId = null;
      this.session.userId = null;
      this.globals.isClient = null;
      this.globals.isChoosed = false;
      this.router.navigate(['/']);
    });
  }
}
