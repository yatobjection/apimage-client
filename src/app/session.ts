import { Injectable } from "@angular/core";

@Injectable()
export class Session {
    clientId:number = null;
    userId:number = null;
}