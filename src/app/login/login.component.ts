import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { IndataService } from '../indata.service';
import { Globals } from '../globals';
import { MatSnackBar } from '@angular/material';
import { RouterLinkWithHref, RouterLink, Router } from '@angular/router';
import { Session } from '../session';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  loginId: number;

  constructor(private indataService: IndataService, private fb: FormBuilder, private globals: Globals, private session: Session, private snackBar: MatSnackBar, public router: Router) { }

  ngOnInit() {
    this.loginForm = this.fb.group({
      mail: [],
      password: []
    });
  }

  onLogin() {
    if (this.globals.isClient == true) {
      this.indataService.loginClient(this.loginForm.value).subscribe(loginClient => {
        this.loginId = loginClient;
        console.log(this.loginId);
        if (this.loginId != null) {
          this.snackBar.open("Connexion avec succès.", null, {
            duration: 2000,
          });
          this.session.clientId = this.loginId;
          this.router.navigate(['/metadatalist']);
        }
        else {
          this.snackBar.open("Erreur d'adresse mail ou de mot de passe, veuillez réessayer.", null, {
            duration: 2000,
          });
        } 
      });
    } else {
      this.indataService.loginUser(this.loginForm.value).subscribe(loginUser => {
        this.loginId = loginUser;
        console.log(this.loginId);
        if (this.loginId != null) {
          this.snackBar.open("Connexion avec succès.", null, {
            duration: 2000,
          });
          this.session.userId = this.loginId;
          this.router.navigate(['/searchclient']);
        }
        else {
          this.snackBar.open("Erreur d'adresse mail ou de mot de passe, veuillez réessayer.", null, {
            duration: 2000,
          });
        }
      });
    }
  }
}
