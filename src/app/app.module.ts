import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule,
        MatCheckboxModule,
        MatSelectModule,
        MatFormFieldModule, 
        MatInputModule,
        MatRadioModule,
        MatToolbarModule,
        MatProgressBarModule,
        MatMenuModule,
        MatSnackBarModule,
        MatDialogModule,
        MatIconModule, 
        MatExpansionModule,
        MatListModule,
        MatCardModule, 
        MatTableModule } from '@angular/material';
import { AppComponent } from './app.component';
import { HttpClientModule } from "@angular/common/http";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SignupComponent } from './signup/signup.component';
import { AppRoutingModule } from './app-routing.module';
import { LoginComponent } from './login/login.component';
import { DisplaymetadataComponent } from './displaymetadata/displaymetadata.component';
import { MetadatalistComponent } from './metadatalist/metadatalist.component';
import { SearchclientComponent } from './searchclient/searchclient.component';
import { EditAccountComponent } from './edit-account/edit-account.component';
import { AlifeFileToBase64Module } from 'alife-file-to-base64';
import { SafePipe } from './safe.pipe';
import { MapComponent } from './map/map.component';

@NgModule({
  declarations: [
    AppComponent,
    SignupComponent,
    LoginComponent,
    DisplaymetadataComponent,
    MetadatalistComponent,
    SearchclientComponent,
    EditAccountComponent,
    MapComponent,
    SafePipe
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    MatRadioModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    MatToolbarModule,
    MatProgressBarModule,
    MatMenuModule,
    MatSnackBarModule,
    MatDialogModule,
    MatIconModule,
    MatExpansionModule,
    AlifeFileToBase64Module,
    MatListModule,
    MatCardModule,
    MatTableModule,
    MatIconModule
  ],
  providers: [ ],
  bootstrap: [AppComponent],
  entryComponents: [MapComponent],
})
export class AppModule { }
