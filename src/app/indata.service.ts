import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { indata } from './indata';

@Injectable({
  providedIn: 'root'
})
export class IndataService {

  constructor(private http: HttpClient) { }

  newClient(body: indata): Observable<indata> {
    return this.http.post<indata>('/client', body);
  }

  loginClient(body: indata):Observable<number> {
    return this.http.post<number>('/client/login', body);
  }

  loginUser(body: indata):Observable<number> {
    return this.http.post<number>('/user/login', body);
  }

  getClient(clientInfo: String): Observable<any> {
    return this.http.get<any>('/client/search?clientInfo=' + clientInfo);
  }

  getAllClient(): Observable<any> {
    return this.http.get<any>('/client/search');
  }

  getUser(userInfo: String): Observable<any> {
    return this.http.get<any>('/user/search?userInfo=' + userInfo);
  }

  getAllUser():Observable<any> {
    return this.http.get<any>('/user/search');
  }

  editClient(body: indata): Observable<indata> {
    return this.http.put<indata>('/client/edit', body);
  }

  editUser(body: indata): Observable<indata> {
    return this.http.put<indata>('/user/edit', body);
  }

  deleteClient(clientId: number): Observable<void> {
    return this.http.delete<void>('/client/delete?clientId=' + clientId);
  }

  deleteUser(userId: number): Observable<void> {
    return this.http.delete<void>('/user/delete?=userId' + userId);
  }

  extractMetadata(body: indata, clientId: number): Observable<indata> {
    return this.http.post<indata>('/extract/metadata?clientId=' + clientId, body);
  }

  getMetadata(clientId: number, isAdmin: Boolean): Observable<any> {
    return this.http.get<any>('/metadata/getList?clientId=' + clientId + '&isAdmin=' + isAdmin);
  }

  editMetadata(mId: number, active: Boolean): Observable<any> {
    return this.http.put<any>('/metadata/edit?mId=' + mId + '&active=' + active, null);
  }

  editAllMetadata(clientId: number, active: Boolean): Observable<any> {
    return this.http.put<any>('/metadata/editAll?clientId=' + clientId + '&active=' + active, null);
  }

  deleteMetadata(mId: number): Observable<void> {
    return this.http.delete<void>('/metadata/delete?mId=' + mId);
  }

  deleteAllMetadata(clientId: number): Observable<void> {
    return this.http.delete<void>('/metadata/deleteAll?clientId=' + clientId);
  }

  convertGPS(latitude: String, longitude: String): Observable<any> {
    return this.http.get<any>('/metadata/convert?latitude=' + latitude + '&longitude=' + longitude);
  }
}
