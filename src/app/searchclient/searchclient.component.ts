import { Component, OnInit } from '@angular/core';
import { IndataService } from '../indata.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog, MatSnackBar } from '@angular/material';
import { DisplaymetadataComponent } from '../displaymetadata/displaymetadata.component';
import { Globals } from '../globals';

@Component({
  selector: 'app-searchclient',
  templateUrl: './searchclient.component.html',
  styleUrls: ['./searchclient.component.css'],
  providers: [ Globals ]
})
export class SearchclientComponent implements OnInit {

  searchClientForm: FormGroup;
  clientdata = [];
  metadata: String;
  edit: Boolean;
  clientForm: FormGroup;
  searchAll: Boolean;

  constructor(private fb: FormBuilder, private indataService: IndataService, public dialog: MatDialog, private snackBar: MatSnackBar, private globals: Globals) { }

  ngOnInit() {
    this.searchClientForm = this.fb.group({
      clientInfo: []
    });
    this.edit = true;
    this.searchAll = false;

    this.clientForm = this.fb.group({
      clientId: [],
      lastName: [],
      firstName: [],
      age: [],
      sex: [],
      phone: [],
      address: [],
      mail: [],
      password: []
    });
    this.clientForm.disable();
  }

  onSearch() {
    if ((this.searchClientForm.value.clientInfo == null) || (this.searchClientForm.value.clientInfo == "")) {
      this.snackBar.open("Veuillez entrer un identifiant ou un nom de client.", null, {
        duration: 2000,
      });
      this.clientdata = [];
    }
    else {
      this.globals.loading = true;
      this.indataService.getClient(this.searchClientForm.value.clientInfo).subscribe(client => {
        if (Number(this.searchClientForm.value.clientInfo)) {
          this.clientdata = [];
          this.clientdata.push(client);
        } else
          this.clientdata = client;
        this.searchAll = false;
        this.globals.loading = false;
        console.log(this.clientdata);
      });
    }
  }

  onSearchAll() {
    this.globals.loading = true;
    this.searchClientForm = this.fb.group({
      clientInfo: []
    });
    this.indataService.getClient("").subscribe(cbAll => {
      this.clientdata = cbAll;
      this.searchAll = true;
      this.globals.loading = false;
      console.log(this.clientdata);
    });
  }

  onEdit(client) {
    this.clientForm = this.fb.group({
      clientId: [client.clientId],
      lastName: [client.lastName],
      firstName: [client.firstName],
      age: [client.age],
      sex: [client.sex],
      phone: [client.phone],
      address: [client.address],
      mail: [client.mail],
      password: [client.password]
    });
    console.log(this.clientdata);

    this.clientForm.enable();
    this.edit = false;
  }

  onValidate() {
    console.log(this.clientForm);
    this.indataService.editClient(this.clientForm.value).subscribe(() => {
      this.edit = true;
      this.clientForm.disable();
      this.snackBar.open("Modifications prises en compte.", null, {
        duration: 2000,
      });
      if (this.searchAll)
        this.onSearchAll();
      else
        this.onSearch();
    });
  }

  onCancel() {
    if (this.searchAll)
      this.onSearchAll();
    else
      this.onSearch();
    this.clientForm.disable();
    this.edit = true;
  }

  getMetadata(clientId) {
    this.dialog.open(DisplaymetadataComponent, {
      height: "95%",
      width: "99%",
      data: {
        id: clientId
      }
    });
  }

  setActive(client, active) {
    this.clientForm = this.fb.group({
      clientId: [client.clientId],
      lastName: [client.lastName],
      firstName: [client.firstName],
      age: [client.age],
      sex: [client.sex],
      phone: [client.phone],
      address: [client.address],
      mail: [client.mail],
      password: [client.password],
      active: [active]
    });

    this.indataService.editClient(this.clientForm.value).subscribe(() => {
      this.clientForm.disable();
      if (active) {
        this.snackBar.open("Compte activé.", null, {
          duration: 2000,
        });
      }
      else {
        this.snackBar.open("Compte désactivé.", null, {
          duration: 2000,
        });
      }
      if (this.searchAll)
        this.onSearchAll();
      else
        this.onSearch();
    });
  }

  onDelete(clientId) {
    this.indataService.deleteClient(clientId).subscribe(() => {
      if (this.searchAll)
      this.onSearchAll();
      else
        this.onSearch();
    });
  }
}
