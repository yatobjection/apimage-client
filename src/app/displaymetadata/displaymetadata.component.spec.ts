import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DisplaymetadataComponent } from './displaymetadata.component';

describe('DisplaymetadataComponent', () => {
  let component: DisplaymetadataComponent;
  let fixture: ComponentFixture<DisplaymetadataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DisplaymetadataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisplaymetadataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
