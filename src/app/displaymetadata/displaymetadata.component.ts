import { Component, OnInit, Inject } from '@angular/core';
import { IndataService } from '../indata.service';
import { MAT_DIALOG_DATA, MatDialog, MatSnackBar } from '@angular/material';
import { Globals } from '../globals';
import { DomSanitizer } from '@angular/platform-browser';
import { MapComponent } from '../map/map.component';

@Component({
  selector: 'app-displaymetadata',
  templateUrl: './displaymetadata.component.html',
  styleUrls: ['./displaymetadata.component.css'],
  providers: [ Globals ]
})
export class DisplaymetadataComponent implements OnInit {

  metadata: String;
  client: any;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private indataService: IndataService, private globals: Globals, private snackBar: MatSnackBar, public dialog: MatDialog, private sanitizer: DomSanitizer) { }

  ngOnInit() {
    this.globals.loading = true;
    console.log(this.data.id);
    this.indataService.getMetadata(this.data.id, true).subscribe(gmbc => {
      this.metadata = gmbc;
      this.onCheck();
      console.log(this.metadata);
      this.globals.loading = false;
    });
    this.indataService.getClient(this.data.id).subscribe(cl => {
      this.client = cl;
      console.log(this.client)
    })
  }

  onDisable(mid) {
    this.globals.loading = true;
    this.indataService.editMetadata(mid, false).subscribe(() => {
      this.indataService.getMetadata(this.data.id, true).subscribe(gmbc => {
        this.metadata = gmbc;
        this.onCheck();
        console.log(this.metadata);
        this.globals.loading = false;
      });
    });
  }

  onDisableAll(client) {
    this.globals.loading = true;
    this.indataService.editAllMetadata(client.clientId, false).subscribe(() => {
      this.indataService.getMetadata(this.data.id, true).subscribe(gmbc => {
        this.metadata = gmbc;
        this.onCheck();
        console.log(this.metadata);
        this.globals.loading = false;
      });
    });
  }

  onEnable(mid) {
    this.globals.loading = true;
    this.indataService.editMetadata(mid, true).subscribe(() => {
      this.indataService.getMetadata(this.data.id, true).subscribe(gmbc => {
        this.metadata = gmbc;
        this.onCheck();
        console.log(this.metadata);
        this.globals.loading = false;
      });
    })
  }

  onEnableAll(client) {
    this.globals.loading = true;
    this.indataService.editAllMetadata(client.clientId, true).subscribe(() => {
      this.indataService.getMetadata(this.data.id, true).subscribe(gmbc => {
        this.metadata = gmbc;
        this.onCheck();
        console.log(this.metadata);
        this.globals.loading = false;
      });
    });
  }

  onDelete(mid) {
    this.globals.loading = true;
    this.indataService.deleteMetadata(mid).subscribe(() => {
      this.indataService.getMetadata(this.data.id, true).subscribe(gmbc => {
        this.metadata = gmbc;
        this.onCheck();
        console.log(this.metadata);
        this.globals.loading = false;
      });
    })
  }

  onDeleteAll(client) {
    this.globals.loading = true;
    this.indataService.deleteAllMetadata(client.clientId).subscribe(() => {
      this.indataService.getMetadata(this.data.id, true).subscribe(gmbc => {
        this.metadata = gmbc;
        this.onCheck();
        console.log(this.metadata);
        this.globals.loading = false;
      });
    })
  }

  onCheck() {
    if (this.metadata.length == 0) {
      this.metadata = null;
    }
  }

  onMap(lat, longi) {
    this.dialog.open(MapComponent, {
      height: "75%",
      width: "50%",
      data: {
        latitude: lat,
        longitude: longi
      }
    });
  }

  onClipboard(val){
    let selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = JSON.stringify(val);;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
    this.snackBar.open("Métadonnées copiées dans le presse-papier.", null, {
      duration: 2000,
    });
  }

  onExport(fileName, myJson){
    var sJson = JSON.stringify(myJson);
    var element = document.createElement('a');
    element.setAttribute('href', "data:text/json;charset=UTF-8," + encodeURIComponent(sJson));
    var fileNameSplited = fileName.split(".");
    element.setAttribute('download', fileNameSplited[0] + ".txt");
    element.style.display = 'none';
    document.body.appendChild(element);
    element.click();
    document.body.removeChild(element);
  }

}
