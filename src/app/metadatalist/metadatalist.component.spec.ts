import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MetadatalistComponent } from './metadatalist.component';

describe('MetadatalistComponent', () => {
  let component: MetadatalistComponent;
  let fixture: ComponentFixture<MetadatalistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MetadatalistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MetadatalistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
