import { Component, OnInit } from '@angular/core';
import { IndataService } from '../indata.service';
import { MatSnackBar, MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { Session } from '../session';
import { Globals } from '../globals';
import { MapComponent } from '../map/map.component';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-metadatalist',
  templateUrl: './metadatalist.component.html',
  styleUrls: ['./metadatalist.component.css'],
  providers: [ Globals ]
})
export class MetadatalistComponent implements OnInit {

  metadata: String;
  selectedFile: any;
  type: [];
  metadatany: any;
  downloadJsonHref: any;

  constructor(private indataService: IndataService, private snackBar: MatSnackBar, public router: Router, private session: Session, private globals: Globals, public dialog: MatDialog, private sanitizer: DomSanitizer) { }

  ngOnInit() {
    this.globals.loading = true;
    this.indataService.getMetadata(this.session.clientId, false).subscribe(metad => {
      this.metadata = metad;
      console.log(this.metadata);
      this.globals.loading = false;
    });
  }

  onFileChanged(event) {
    this.selectedFile = event[0];
    console.log(this.selectedFile);
    this.type = this.selectedFile.type.split("/");
    console.log(this.type);
  }

  onUpload() {
    this.globals.loading = true;
    this.indataService.extractMetadata(this.selectedFile, this.session.clientId).subscribe(meta => {
      this.metadatany = meta;
      console.log(this.metadatany);
      this.snackBar.open("Extraction des métadonnées avec succès.", null, {
        duration: 2000,
      });
      this.ngOnInit();
      this.onCancel();
      this.globals.loading = false;
    });
  }
  
  onCancel() {
    this.selectedFile = null;
  }

  desactivate(mId: number) {
    this.globals.loading = true;
    this.indataService.editMetadata(mId, false).subscribe(() => {
      this.indataService.getMetadata(this.session.clientId, false).subscribe(metad => {
        this.metadata = metad;
        console.log(this.metadata);
        this.globals.loading = false;
     });
    });
  }

  onMap(lat, longi) {
    this.dialog.open(MapComponent, {
      height: "75%",
      width: "50%",
      data: {
        latitude: lat,
        longitude: longi
      }
    });
  }

  onClipboard(val){
    let selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = JSON.stringify(val);;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
    this.snackBar.open("Métadonnées copiées dans le presse-papier.", null, {
      duration: 2000,
    });
  }

  onExport(fileName, myJson){
    var sJson = JSON.stringify(myJson);
    var element = document.createElement('a');
    element.setAttribute('href', "data:text/json;charset=UTF-8," + encodeURIComponent(sJson));
    var fileNameSplited = fileName.split(".");
    element.setAttribute('download', fileNameSplited[0] + ".txt");
    element.style.display = 'none';
    document.body.appendChild(element);
    element.click();
    document.body.removeChild(element);
  }
}