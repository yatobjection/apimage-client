import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { IndataService } from '../indata.service';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {

  gps: any;
  url: String;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private indataService: IndataService) { }

  ngOnInit() {
    console.log(this.data.latitude);
    console.log(this.data.longitude);

    this.indataService.convertGPS(this.data.latitude, this.data.longitude).subscribe(coord => {
      this.gps = coord;
      this.url = "http://maps.google.com/maps?q=" + this.gps.latitude + "," + this.gps.longitude + "&z=15&output=embed";
      console.log(this.url);
    });
  }

}
