import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Globals } from './globals';
import { IndataService } from './indata.service';
import { Session } from './session';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [ Globals, Session ]
})
export class AppComponent {
  title = 'apimage-front';
  
  constructor(private router: Router, private globals: Globals, private session: Session, private indataService: IndataService, private snackBar: MatSnackBar) { }

  ngOnInit() {
    if (!this.session.clientId && !this.session.userId)
      this.router.navigate(['/']);
    this.globals.isClient = null;
    this.globals.isChoosed = false;
  }

  onClient() {
    this.globals.isClient = true;
    this.globals.isChoosed = true;
  }

  onUser() {
    this.globals.isClient = false;
    this.globals.isChoosed = true;
  }

  onLogout() {
    this.snackBar.open("Déconnexion avec succès.", null, {
      duration: 2000,
    });
    this.session.clientId = null;
    this.session.userId = null;
    this.globals.isClient = null;
    this.globals.isChoosed = false;
    this.router.navigate(['/']);
  }
}

